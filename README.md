## Network 
    tcpdump -i <interface> port 443
    netstat -plutone
## FIND
    find / -type f -mmin -5
    find / -type f -size +4M
## ZIP
    zcat <folder>
    zgrep "<pattern>" <folder>
## systemctl 
    systemctl list-units
    systemctl list-dependencies sshd.service
    systemctl list-unit-files
## journalctl
    journalctl -u <service>
    journalctl --vacuum-time=1seconds
## loginctl
    loginctl unlock-session [ID]
    loginctl list-sessions
## attr
    lsattr <folder>
## vim
Maj+V = Sélectioner
Y = copier
P = coller
:<ligne_début>,<ligne_fin>s/<pattern_ancien>/<pattern_nouveau>/g
##  Postgres
    -- Ajout utilisateur --
    psql postgres
    CREATE ROLE "<name>" LOGIN;
    ALTER USER "<name>" ENCRYPTED PASSWORD '<password>';
    \c \<BDD>
    GRANT "<group>" TO "<name>";
    exit
    cd /etc/postgresql/12/main/
    vim pg_hba.conf
    systemctl reload postgresql@12-main.service
    -- Test login --
    psql -h <machine> -U <utilisateur> -W <base>
    -- List group/user -- 
    \du
## swap 
    for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | sort -k 2 -n -r | head
## ram 
    ps aux  | awk '{print $6/1024 " MB\t\t" $11}'  | sort -n
    ps -eo pmem,vsize,cmd | grep -v '\[' | awk 'NR>2{mem[$3]+=$2}END {for(k in mem) print k " " mem[k]/1024000};' | sort -gk 2 | tail -n 10
## pidstat
    pidstat
## cpu
    mpstat 
## disque 
    badblock
    lsblk
    df -h
    ncdu /
    smartctl 
## process
    lsof -p <PID>
    strace -p <PID>
## apt-mark 
    apt-mark showhold
    apt-mark hold
    apt-mark unhold
## crowdsec
    cscli decisions list
    cscli decisions list -a 
    cscli decisions list -a --origin crowdsec
    cscli parsers list
    cscli scenarios list
    cscli collections list
    cscli alerts list
    /usr/share/crowdsec/wizard.sh -c
    cscli lapi register http://10.1.2.141:8080/
    cscli lapi status
    systemctl reload crowdsec
## mysql 
    mysqldump -u [user] -p [database_name] > [filename].sql
    mysql -u [user] -p [database_name] < [filename].sql

## monit 
    monit summary
    monit unmonitor <servicemonit>
    monit monitor <servicemonit>
## Compter le nombre d'ip dans un fichier 
    cat /var/log/haproxy.log |egrep -o '[[:digit:]]{1,3}(\.[[:digit:]]{1,3}){3}' | sort | uniq -c | sort
## Voir les site dans haproxy
    grep acl /etc/haproxy/haproxy.cfg |grep "hdr(host) eq" |awk '{ print $5}'